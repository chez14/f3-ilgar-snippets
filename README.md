# F3-Ilgar Migration Snippets

This extension provide quick snippets for [Ilgar Migration
package](https://packagist.org/packages/chez14/f3-ilgar).

## Install

To install this extension you just need to copy this line and then run it on the
Quick Open menu (<kbd>Ctrl</kbd> + <kbd>P</kbd>):

```
ext install chez14.f3-ilgar-snippets
```

## Available Snippets

- `ilgar:new` in PHP \
  You'll need to type `<?php` first before the snippets reccomendation comes up.


## Suggestions and Contacts
Please send suggestions on the repo:
https://gitlab.com/chez14/f3-ilgar-snippets.